﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherStationTracker.Models
{
    public class WeatherStation
    {
        //demonstrates setting auto implemented property initializers
        public int? Id { get; set; } = 0;
        public string Name { get; set; } = "unknown";
        public string Location { get; set; } = "Somewhere";
        public double? Altitude { get; set; } = 0;
        public bool IsBuilt { get; } = false;
        public bool AboveSeaLevel => Altitude > 0; 
        public WeatherForecast DailyForecast { get; set; } = new WeatherForecast() { };

        public WeatherStation(bool built = false)
        {
            IsBuilt = built;
        }

        public static List<WeatherStation> GetRandomWeatherStations()
        {
            string[] Summaries = new[]
                 {
                     "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
                 };

            string[] Cities = new[]
                 {
                     "Madrid", "Beuno Aires", "Terahn", "Karachi", "Montreal", "Bergen", "Port Elizabeth", "Quebec", "St Peteresburg", "Berlin"
                 };

            Random rng = new Random();
            int auto = 1;

            List<WeatherForecast> forecasts = Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Id = auto++,
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }).ToList();

            forecasts.Add(new WeatherForecast() { Id = auto++, Date = DateTime.Now.AddDays(1), Summary = Summaries[rng.Next(Summaries.Length)], TemperatureC = rng.Next(-20, 55) });
            forecasts.Add(null);

            auto = 1;

            //constructor used for these objects to initialize the readonly bool
            List<WeatherStation> stations = Enumerable.Range(1, 40).Select(index => new WeatherStation(true)
            {
                Id = auto++,
                Name = $"{Cities[rng.Next(Cities.Length)]} Station ",
                Altitude = rng.Next(-1000, 5000),
                Location = Cities[rng.Next(Cities.Length)],
                DailyForecast = forecasts[rng.Next(forecasts.Count)]
            }).ToList();

            //constructor not used for this object readonly bool defaults to false
            stations.Add(new WeatherStation
            {
                Id = auto++,
                Name = $"{Cities[rng.Next(Cities.Length)]} Station ",
                Altitude = rng.Next(-1000, 5000),
                Location = Cities[rng.Next(Cities.Length)],
                DailyForecast = forecasts[rng.Next(forecasts.Count)]
            });

            return stations;

        }


    }
}
