# Weather Station Tracker

.NET Core Console Application

Intended for learning Prominent C# features as part of module  of Noroff Accelerate .NET Fullstack short course.

Demonstrates the use of various important C# features


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run

File output will default to the bin/debug folder of the project

### Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)




